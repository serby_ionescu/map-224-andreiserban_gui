package com.example.ex1;
import com.example.ex1.controller.Controller;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
public class HelloController {
    @FXML
    private TextField first_name_textfield;
    @FXML
    private TextField last_name_textfield;
    Controller controler;
    @FXML
    protected void onLoginButtonClick()
    {
        String first_name=first_name_textfield.getText();
        String last_name=last_name_textfield.getText();
        //System.out.println(first_name+" "+ last_name);
        controler.changeCurrentUser(first_name,last_name);
    }

    public void setController(Controller ctrl)
    {
        controler=ctrl;
    }
}

