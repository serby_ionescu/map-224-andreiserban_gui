package com.example.ex1.controller;

public class InvalidMessageSenderException extends RuntimeException {
    InvalidMessageSenderException(String message) {
        super(message);
    }
}
