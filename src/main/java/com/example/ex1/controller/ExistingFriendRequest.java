package com.example.ex1.controller;

public class ExistingFriendRequest extends RuntimeException{

    public ExistingFriendRequest(String msg)
    {
        super(msg);
    }
}
