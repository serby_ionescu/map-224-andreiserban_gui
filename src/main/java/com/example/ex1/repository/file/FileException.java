package com.example.ex1.repository.file;

public class FileException extends RuntimeException {
    public FileException(String message) {
        super(message);
    }
}
