package com.example.ex1;
import com.example.ex1.controller.Controller;
import com.example.ex1.model.validators.FriendshipValidator;
import com.example.ex1.model.validators.MessageValidator;
import com.example.ex1.model.validators.UserValidator;
import com.example.ex1.repository.database.FriendshipDatabaseRepository;
import com.example.ex1.repository.database.MessageDatabaseRepository;
import com.example.ex1.repository.database.UserDatabaseRepository;
import com.example.ex1.service.FriendshipService;
import com.example.ex1.service.MessageService;
import com.example.ex1.service.UserService;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import java.io.IOException;
public class HelloApplication extends Application {

    UserValidator userValidator = new UserValidator();
    FriendshipValidator friendshipValidator = new FriendshipValidator();
    MessageValidator messageValidator = new MessageValidator();
    UserDatabaseRepository userRepository = new UserDatabaseRepository("jdbc:postgresql://localhost:5432/academic",
            "postgres", "serbyetare", userValidator);
    FriendshipDatabaseRepository friendshipRepository = new FriendshipDatabaseRepository("jdbc:postgresql://localhost:5432/academic",
            "postgres", "serbyetare", friendshipValidator);
    MessageDatabaseRepository messageRepository = new MessageDatabaseRepository("jdbc:postgresql://localhost:5432/academic",
            "postgres", "serbyetare", messageValidator);
    UserService userService = new UserService(userRepository);
    FriendshipService friendshipService = new FriendshipService(friendshipRepository);
    MessageService messageService = new MessageService(messageRepository);
    Controller controller = new Controller(userService, friendshipService, messageService);
    public static void main(String[] args) {
        //System.out.println("ok");
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        //System.out.println("ok");
        //String fileN = ApplicationContext.getPROPERTIES().getProperty("data.tasks.messageTask");
        //messageTaskService.getAll().forEach(System.out::println);
        initView(primaryStage);
        primaryStage.setWidth(800);
        primaryStage.show();
    }

    private void initView(Stage primaryStage) throws IOException {

        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("hello-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 320, 240);
        primaryStage.setTitle("Hello!");
        primaryStage.setScene(scene);
        primaryStage.show();
        HelloController HelloController = fxmlLoader.getController();
        HelloController.setController(controller);
    }
}