package com.example.ex1.controller;

public class NonExistingFriendRequest extends RuntimeException{
    public NonExistingFriendRequest(String msg)
    {
        super(msg);
    }
}
