package com.example.ex1.controller;

public class RejectionException extends RuntimeException{
    public RejectionException(String msg)
    {
        super(msg);
    }
}
